import pygame
import random
import math

# Initialize Pygame
pygame.init()

# Set up the game window
window_width = 1280
window_height = 720
window = pygame.display.set_mode((window_width, window_height))
pygame.display.set_caption("Linksmas kamuoliukas")

# Define colors
BLACK = (0, 0, 0)
RED = (255, 0, 0)

# Define the ball
ball_radius = 20
ball_x = window_width // 2
ball_y = window_height // 2
ball_speed_x = 0
ball_speed_y = 0
is_throwing = False
is_picked_up = False
throw_start_pos = (0, 0)

# Define the gravity
gravity = 1.2  # Increase the gravity value for a stronger force

# Define the bouncing factors
bouncing_factor_x = 0.8  # Decrease the bouncing factor for a smaller speed after bouncing
bouncing_factor_y = 0.8  # Decrease the bouncing factor for a smaller speed after bouncing

# Define the time interval for calculating the throwing force
throw_interval = 0.5  # Adjust the time interval as desired (in seconds)

# Define wind resistance
wind_resistance = 0.005  # Adjust the wind resistance value as desired

# Calculate the throw speed and direction based on the ball's velocity
def calculate_throw_speed_and_direction(speed_x, speed_y):
    throw_speed = math.sqrt(speed_x ** 2 + speed_y ** 2)
    throw_direction = math.atan2(-speed_y, speed_x)  # Reverse the y component for correct direction
    return throw_speed, throw_direction

# Game loop
running = True
clock = pygame.time.Clock()

while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

        # Handle mouse events for picking up and releasing the ball
        if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            if math.sqrt((event.pos[0] - ball_x) ** 2 + (event.pos[1] - ball_y) ** 2) <= ball_radius:
                is_picked_up = True
                throw_start_pos = event.pos
        
        if event.type == pygame.MOUSEBUTTONUP and event.button == 1:
            if is_picked_up:
                is_picked_up = False
                is_throwing = True
                throw_end_pos = event.pos
                
                throw_distance = math.hypot(throw_end_pos[0] - throw_start_pos[0], throw_end_pos[1] - throw_start_pos[1])
                throw_speed = throw_distance / throw_interval
                
                speed_factor = 0.05  # Adjust the speed factor as desired
                ball_speed_x = (throw_end_pos[0] - throw_start_pos[0]) * throw_speed * speed_factor / throw_distance
                ball_speed_y = (throw_end_pos[1] - throw_start_pos[1]) * throw_speed * speed_factor / throw_distance
    
    # Apply gravity
    ball_speed_y += gravity
    
    # Apply wind resistance
    ball_speed_x -= ball_speed_x * wind_resistance
    
    # Update the ball position
    if is_picked_up:
        ball_x, ball_y = pygame.mouse.get_pos()
    
    else:
        ball_x += ball_speed_x
        ball_y += ball_speed_y
    
    # Check for collision with walls
    if ball_x - ball_radius < 0:
        ball_x = ball_radius
        ball_speed_x *= -bouncing_factor_x
    elif ball_x + ball_radius > window_width:
        ball_x = window_width - ball_radius
        ball_speed_x *= -bouncing_factor_x
    if ball_y - ball_radius < 0:
        ball_y = ball_radius
        ball_speed_y *= -bouncing_factor_y
    elif ball_y + ball_radius > window_height:
        ball_y = window_height - ball_radius
        ball_speed_y *= -bouncing_factor_y
    
    # Check if the ball is thrown
    if is_throwing:
        throw_speed, throw_direction = calculate_throw_speed_and_direction(ball_speed_x, ball_speed_y)
        ball_speed_x = throw_speed * math.cos(throw_direction)
        ball_speed_y = -throw_speed * math.sin(throw_direction)  # Reverse the y component for correct direction
        is_throwing = False
    
    # Update the window
    window.fill(BLACK)
    pygame.draw.circle(window, RED, (int(ball_x), int(ball_y)), ball_radius)
    
    pygame.display.update()
    
    clock.tick(60)  # Limit the frame rate to 60 FPS

# Quit the game
pygame.quit()
